#include <stdio.h>

int main(int argc, char *argv[]) {
    if(argc < 2){
        fprintf(stderr, "Missing parameter, input file name.");
        return 1;
    }
    FILE *infile = fopen(argv[1],"rb");
    if (!infile) {
        fprintf (stderr, "error: file open failed '%s'.\n", argv[1]);
        return 1;
    }
    unsigned char buf[2] = {0};
    int bytes; // number of bytes read.

    // The first two bytes should be the starting address
    bytes = fread (buf, 1, 2, infile);
    if(bytes < 2){
        fprintf (stderr, "error: Expected 16 bit address at the start of the file.\n");
        return 1;
    }
    int addrl = buf[0];
    int addrh = buf[1];
    int addr = addrl + 256*addrh;

    int lineNo = 9000;
    printf("\n%d DATA %u\n",lineNo,addr);
    lineNo += 10;
    int dataCount = 0;
    // Next read all the other bytes
    while ((bytes = fread (buf, 1, 1, infile)) >0) {
        if(dataCount%10 == 0) {
            printf("\n%d DATA ",lineNo);
            lineNo += 10;
        } else {
            printf(", ");
        };
        printf ("%u", buf[0]);
        dataCount++;
    }
    fclose(infile);
    puts("\n");

    printf("%d REM PUT THE DATA INTO RAM\n", lineNo);
    lineNo += 10;
    printf("%d READ AD\n", lineNo);
    lineNo += 10;
    printf("%d FOR I=AD TO AD+%d\n", lineNo, dataCount-1);
    lineNo += 10;
    printf("%d READ D:POKE I,D\n", lineNo);
    lineNo += 10;
    printf("%d NEXT\n", lineNo);
    lineNo += 10;



    // Exit insert data mode and return to the start address
    printf("%d REM SET USR ADDR.\n", lineNo);
    lineNo += 10;
    printf("%d POKE 11, %u: POKE 12, %u\n", lineNo,addrl,addrh);
    lineNo += 10;
    printf("%d REM RUN THE PROGRAM\n", lineNo);
    lineNo += 10;
    printf("%d X = USR(X)\n", lineNo);
    lineNo += 10;

    return 0;
}
