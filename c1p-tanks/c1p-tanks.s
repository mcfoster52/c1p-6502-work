; C1P TANK BATTLE GAME
; MARTIN C. FOSTER
; cfoster@mcfse.com
; APRIL 9, 2023
;
; ROM SUBROUTINES 
GET_KBD         = $FEED ; WAIT KEY TO ACCUMULATOR
CHROUT          = $A8E5
CTRL_C_DISABLE  = $212  ; 01 = DISABLE CTRL-C
LAST_CR         = $0E   ; CHARACTERS SINCE LAST CR
POLLED_KBD      = $DF00 ;
SCREEN 	        = $D000
SCREEN_START    = $D085
REBOOT 			= $FF00 

TML     = $E0
TMH     = $E1
TML2    = $E2
TMH2    = $E3

; KEYBOARD CODES
KEY_RIGHT   = $FC
KEY_LEFT    = $FA
KEY_MOVE    = $F8
KEY_SHOOT   = $BE ; $FF
KEY_NONE    = $FE
KEY_ESC     = $DE

; GAME CONSTANTS
SCR_HT  =$19
SCR_WD  =$20
MAX_X   = 22
MAX_Y   = 26
BRICK   =$BB
TANK0   =$F8
;BULLET  =$6F
EXPLOD1 =$BC
EXP_LEN =$1F

;.ORG $12FE  
;.BYTE $00, $13  ; START ADDRESS 4896
.ORG $0220  
.BYTE $22, $02  ; START ADDRESS $0300
START:
        JSR SCRAMBLE_SCR
        JSR SHOW_DIRECTIONS
GAME_START:
        LDA #$00
        STA F_DONE  ; CLEAR GAME DONE FLAG
        STA SCRN    ; SET SCREEN# 0
        JSR SHOW_SCREEN
LOOP1:
        LDA F_DONE
        BNE GAME_DONE ; NOT ZERO, DONE
        INC L_COUNT ; LOOP COUNTER
        LDA L_COUNT
        AND #$07

        CMP #$07
        BNE GAME1
        JSR JOY2_CHK
        LDA USER_COUNT
        BEQ GAME_DONE
        LDA INVADERS
        BEQ GAME_DONE
GAME1:
        LDA L_COUNT
        CMP #$0F    ; EVERY 15TH UPDATE
        BCC GAME2
        JSR COMP_MOVE
        LDA #$00    ; RESET LOOP COUNTER
        STA L_COUNT
GAME2:
        ; SHOW TANKS AFTER UPDATES AND 	
        ; BEFORE DELAY TO REDUCE FLICKER
        JSR SHOW_TANKS 
        JSR MOVE_BULLETS
        LDX #3      ; 60 = 1 SECOND
        JSR DELAY
        JMP LOOP1
GAME_DONE:
        JSR SHOW_WINNER
LOOP2:
        JSR GET_KBD
        CMP #$59    ; Y KEY?
        BEQ GAME_RESET
        CMP #$20
        BCS GAME_END ; ANY OTHER KEY
        JMP LOOP2
GAME_RESET:
        JSR RESET
        JMP GAME_START        
GAME_END:
        ; JSR SHOW_ACCUM ; DEBUG
        ; RTS
        JMP REBOOT 
        BRK

;======================================
; DELAY RASTER SCAN 50HZ PAL, 60HZ NTSC
DELAY:
        ; SAVE X, A REGISTERS
        PHA
        TXA
        PHA
        TYA
        PHA
        ;
        LDY #$14 ; DELAY MULTIPLIER
        LDX #$00 ; 00 = MAX DELAY
D1:
        DEX
        BNE D1         
        DEY
        BNE D1
        ;
        ; RESTORE X, A REGISTERS
        PLA
        TAY
        PLA
        TAX
        PLA
        RTS

;======================================
; CHECK JOYSTICK AND KEYBOARD
; CHECK FOR KEY PRESS
JOY2_CHK:
    ; PUT JOYSTICK CODE HERE.
    ; WE  WILL SKIP IT FOR NOW.
USER_KEY:
        LDA POLLED_KBD      ; KEY CODE IS IN ACCUMULATOR
        CMP #KEY_NONE
        BEQ CHK_DONE
        ; JSR SHOW_ACCUM
        TAY                 ; SAVE KEY CODE TO Y REGISTER
        LDA TANKS_LIST+2    ; GET USER TANK DIRECTION
        CMP #$08            ; ALIVE?
        BCC CHK_LEFT ; WE STILL ALIVE..
        LDA #$FF ; WE ARE DEAD, DONE FLAG
        STA F_DONE
        JMP CHK_DONE
CHK_LEFT:
        TYA
        CMP #KEY_LEFT        ; LEFT?
        BNE CHK_RIGHT   
DO_LEFT:
        LDY TANKS_LIST+2
        DEY
        TYA
        AND #$07
        STA TANKS_LIST+2
        JMP CHK_DONE
CHK_RIGHT:
        CMP #KEY_RIGHT        ; RIGHT?
        BNE CHK_MOVE
DO_RIGHT:
        LDY TANKS_LIST+2
        INY
        TYA
        AND #$07
        STA TANKS_LIST+2
        JMP CHK_DONE
CHK_MOVE:
        CMP #KEY_MOVE
        BNE CHK_FIRE
        LDX #$00        ; MOVE USER TANK
        JSR MOVE_TANK
        JMP CHK_DONE    
CHK_FIRE:
        CMP #KEY_SHOOT
        BNE CHK_ESC
        LDX #$00        ; USER TANK INDEX
        JSR START_BULLET
        JMP CHK_DONE    
CHK_ESC:
        CMP #KEY_ESC
        BNE CHK_DONE
        LDA #$FF
        STA F_DONE
CHK_DONE:
        RTS             ; RETURN FROM SUBROUTINE

;=================================================
; PAINT ALL TANKS IN THE LIST ON THE SCREEN
; FIRST TANK CHR VALUE = 134 OR $86
SHOW_TANKS:
        LDX #$00    ; ZERO TANK INDEX
        ; ZERO TANK COUNTERS
        STX USER_COUNT
        STX INVADERS
ST1:
        LDA TANKS_LIST+2,X
        CMP #$FF
        BEQ ST3     ; ALL DONE
        CMP #$08
        BEQ ST2     ; TANK IS DEAD
        JSR COUNT_TANK
        ; CHECK SCREEN AFTER COUNTING THE TANK
        LDA TANKS_LIST+3,X
        CMP SCRN
        BNE ST2     ; NEXT TANK
        LDA TANKS_LIST+2,X
        STA TEMP_DIR
        LDA TANKS_LIST,X        ; GET X VALUE
        STA TEMP_X
        LDA TANKS_LIST+1,X      ; GET Y VALYE
        STA TEMP_Y
        JSR GET_POS
        LDY #$00
        LDA TEMP_DIR
        CMP #$08
        BCS ST1A	; TANK IS EXPLODING
        LDA (TML),Y
        CMP #TANK0
        BCS ST1B
        CMP #EXPLOD1
        BCC ST1B
        ; TANK IS EXPLODING
        LDA #EXPLOD1
        STA TANKS_LIST+2,X
ST1A:
        INC TANKS_LIST+2,X
        LDA TANKS_LIST+2,X
        STA (TML),Y
        CMP #EXPLOD1+EXP_LEN
        BCC ST2	; NEXT TANK
        ; DONE EXPLODING
        ;JSR SHOW_ACCUM ; DEBUG
        LDA #$08	; MARK AS DEAD
        STA TANKS_LIST+2,X
        LDA #$20 ; ERASE
        STA (TML),Y
        JMP ST2      
        ; GET DIRECTION   
ST1B:   LDA TANKS_LIST+2,X     
        ; DIRECTION TO TANK CHARACTER
        CLC
        ADC #TANK0              
        STA (TML),Y
ST2:    INX
        INX
        INX
        INX
        BNE ST1
ST3:
        RTS
        
;======================================
; INCREMENT TANK COUNTER
COUNT_TANK:
        CPX #$00 ; USER TANK?
        BNE CNT1
        INC USER_COUNT
        JMP CNT_DONE 
CNT1:
        INC INVADERS
CNT_DONE:
        RTS

;======================================
; START BULLET
; INPUTS, X=TANK STARTING THE BULLET
START_BULLET:
        PHA
        TXA
        PHA
        LDA TANKS_LIST,X
        STA TEMP_X
        LDA TANKS_LIST+1,X
        STA TEMP_Y
        LDY TANKS_LIST+2,X
        STY TEMP_DIR
        ; FIND FREE BULLET
        LDX #$00
SB_LOOP:
        LDA BULLETS_LIST,X
        CMP #$FF	; LIST IS FULL
        BEQ SB_DONE
        LDA BULLETS_LIST+2,X
        CMP #$08
        BCS SB_FOUND	; BULLET SLOT IS AVAILABLE
        INX
        BEQ SB_DONE
        INX
        BEQ SB_DONE
        INX
        BEQ SB_DONE
        JMP SB_LOOP
SB_FOUND:
        ; ADD THE BULLET TO THE LIST
        ; AT CURRENT TANK LOCATION AND DIRECTION
        ;JSR SHOW_ACCUM
        LDA TEMP_X
        STA BULLETS_LIST,X
        ;JSR SHOW_ACCUM
        LDA TEMP_Y
        STA BULLETS_LIST+1,X
        ;JSR SHOW_ACCUM
        LDA TEMP_DIR
        STA BULLETS_LIST+2,X
        ;JSR SHOW_ACCUM
SB_DONE:
        PLA
        TAX
        PLA
        RTS

;=================================================
; GO THROUGH BULLET LIST AND UPDATE THE POSITIONS
; AFFECTS REGISTERS A, X, Y
MOVE_BULLETS:
        LDX #$00	; BULLET INDEX
MB_LOOP:
        LDA BULLETS_LIST,X
        CMP #$FF	; LIST IS FULL?
        BEQ MB_D1	; ALL DONE
        STA TEMP_X

        LDA BULLETS_LIST+2,X
        CMP #$08	; NO BULLET FLAG
        BCS MB_N1	; NO BULLET HERE
        STA TEMP_DIR
        LDA BULLETS_LIST+1,X
        STA TEMP_Y
        JSR GET_POS
        LDY #$00
        LDA #$20
        STA (TML),Y	; BLANK THE BULLET
        LDA TEMP_DIR	
; MOVE THE BULLET POINTED TO BY THE X REGISTER
MV_BULLET:
        CMP #$00
        BNE MV1
        ; NORTH
        LDA TEMP_Y
        BEQ MB_K1 ;HIT EDGE OF SCREEN
        DEC TEMP_Y
        JMP CHECK_HIT
MV1:
        CMP #$01
        BNE MV2
        ; NORTH EAST
        LDA TEMP_Y
        BEQ MB_K1 
        DEC TEMP_Y
        LDA TEMP_X
        CMP #MAX_X
        BCS MB_K1 
        INC TEMP_X
        JMP CHECK_HIT
MV2:
        CMP #$02
        BNE MV3
        ; EAST
        LDA TEMP_X
        CMP #MAX_X
        BCS MB_K1 
        INC TEMP_X
        JMP CHECK_HIT
MV3:
        CMP #$03
        BNE MV4
        ; SOUTH EAST
        LDA TEMP_Y
        CMP #MAX_Y
        BCS MB_K1
        INC TEMP_Y
        LDA TEMP_X
        CMP #MAX_X
        BCS MB_K1 
        INC TEMP_X
        JMP CHECK_HIT
MB_D1:  ; FIX BRANCH TOO LONG
        JMP MB_DONE
MB_K1:
        JMP KILL_BULLET
MB_N1:
        JMP MB_NEXT
MV4:
        CMP #$04
        BNE MV5
        ; SOUTH
        LDA TEMP_Y
        CMP #MAX_Y
        BCS KILL_BULLET
        INC TEMP_Y
        JMP CHECK_HIT
MV5:
        CMP #$05
        BNE MV6
        ; SOUTH WEST
        LDA TEMP_Y
        CMP #MAX_Y
        BCS KILL_BULLET
        INC TEMP_Y
        LDA TEMP_X
        BEQ KILL_BULLET 
        DEC TEMP_X
        JMP CHECK_HIT
MV6:
        CMP #$06
        BNE MV7		; MUST BE 7
        ; WEST
        LDA TEMP_X
        BEQ KILL_BULLET 
        DEC TEMP_X
        JMP CHECK_HIT
MV7:
        ; NORTH WEST
        LDA TEMP_Y
        BEQ KILL_BULLET
        DEC TEMP_Y
        LDA TEMP_X
        BEQ KILL_BULLET 
        DEC TEMP_X
CHECK_HIT:
        LDA TEMP_X
        STA BULLETS_LIST,X ; UPDATE COL
        LDA TEMP_Y
        STA BULLETS_LIST+1,X ; UPDATE ROW
        
        JSR GET_POS
        LDY #$00
        LDA (TML),Y
        
        CMP #BRICK
        BEQ KILL_BULLET ; HIT A WALL
        
        CMP #TANK0
        BCC MB_DRAW
        ;CMP #TANK0 + 8   ; TANK0+8 == $100, INVALID
        ;BCS MB_DRAW
        ; MUST HAVE HIT A TANK
        LDA #EXPLOD1
        STA (TML),Y
        JMP KILL_BULLET	
MB_DRAW:
        TXA     ; SAVE X
        PHA
        LDA BULLETS_LIST+2,X ; GET DIRECTION
        TAX
        LDA BULLETS,X   ; GET THE BULLET CHARACTER
        STA (TML),Y
        PLA     
        TAX     ; RESTORE X
        JMP MB_NEXT	
KILL_BULLET: ; END OF LIFE FOR THIS BULLET
        LDA #$08
        STA BULLETS_LIST+2,X ; REMOVE BULLET FROM THE LIST
MB_NEXT:
        INX
        BEQ MB_DONE ; X REG OVERFLOW
        INX
        BEQ MB_DONE ; X REG OVERFLOW
        INX
        BEQ MB_DONE ; X REG OVERFLOW		
        JMP MB_LOOP
MB_DONE:
        RTS

;==================================================
; ADVANCE TANK BASED ON DIRECTION
; TANK INDEX IN X REGISTER 0,3,6,9,12
MOVE_TANK:
        PHA     ; SAVE A
        TYA     ; SAVE Y
        PHA
        TXA     ; SAVE X REG
        PHA
        LDA TANKS_LIST,X    ; GET X VALUE
        STA TEMP_X
        LDA TANKS_LIST+1,X  ; GET Y VALYE
        STA TEMP_Y
        JSR GET_POS
        
        ; ERACE TANK
        LDY #$00
        LDA #$20
        STA (TML),Y
        
        LDA TANKS_LIST+2,X
        ; JSR SHOW_ACCUM
        CMP #$00
        BNE MT1
        JSR MOVE_N
        JMP MT_DONE
MT1:    CMP #$01
        BNE MT2
        JSR MOVE_N
        JSR MOVE_E
        JMP MT_DONE
MT2:    CMP #$02
        BNE MT3
        JSR MOVE_E
        JMP MT_DONE
MT3:    CMP #$03
        BNE MT4
        JSR MOVE_S
        JSR MOVE_E
        JMP MT_DONE
MT4:    CMP #$04
        BNE MT5
        JSR MOVE_S
        JMP MT_DONE
MT5:    CMP #$05
        BNE MT6
        JSR MOVE_S
        JSR MOVE_W
        JMP MT_DONE
MT6:    CMP #$06
        BNE MT7
        JSR MOVE_W
        JMP MT_DONE
MT7:    CMP #$07
        BNE MT_DONE
        JSR MOVE_N
        JSR MOVE_W
MT_DONE:   
        PLA     ; RESTORE X REG
        TAX
        CMP #$00
        BNE MTD2
        ; FOR TANK #0, CHECK NEW SCREEN
        LDA TEMP_X
        CMP #MAX_X-1
        BCC MTD1 ; TANK X < MAX_X-1
        ; MOVE USER TANK TO SCRN2
        LDA #$01
        STA SCRN
        STA TANKS_LIST+3
        ; STA TANKS_LIST ; Set X to 1
        STA TEMP_X ; MOVE TO 1 ON THE NEXT SCREEN
        JSR SHOW_SCREEN
        JMP MTD2        
MTD1:
        CMP #$01 
        BCS MTD2 ; TANK X > 1
        ; MOVE USER TANK TO SCRN1
        LDA #$00
        STA SCRN
        STA TANKS_LIST+3
        LDA #MAX_X-2
        ;STA TANKS_LIST ; Set X to SCREEN WIDTH-2
        STA TEMP_X ; MOVE TO RIGHT SIDE SCREEN 0
        JSR SHOW_SCREEN
MTD2:
        LDA TEMP_X
        STA TANKS_LIST,X    ; SET X VALUE
        LDA TEMP_Y
        STA TANKS_LIST+1,X  ; SET Y VALYE

        PLA     ; RESTORE Y
        TAY
        PLA     ; RESTORE A
        RTS

;==================================================
; COMPUTER MOVE
COMP_MOVE:
        LDX #$04 ; FIRST COMPUTER TANK
COMP_LOOP:
        LDA TANKS_LIST+3,X
        CMP #$04 ; 
        BCC COMP_CONT ; GOOD SCREEN?
        JMP COMP_DONE ; ALL DONE
COMP_CONT:
        CMP SCRN
        BNE COMP_NEXT
        LDA TANKS_LIST+2,X
        CMP #$08    ; CHECK DIRECTION
        BCS COMP_NEXT ; DEAD?
        TAY 
        JSR RAND8
        AND #$03 ; JUST USE LOWER 2 BITS
        BNE COMP1
        ; A = 0 ROTATE LEFT
        DEY
        TYA
        AND #$07
        STA TANKS_LIST+2,X
        JMP COMP3
COMP1:
        CMP #$01
        BNE COMP2
        ; A = 1 ROTATE RIGHT
        LDY TANKS_LIST+2,X
        INY
        TYA
        AND #$07
        STA TANKS_LIST+2,X
        JMP COMP3
COMP2:
        ; A > 1 MOVE
        JSR MOVE_TANK
COMP3:
        JSR CHK_AIM
        CMP #$00
        BEQ COMP_NEXT 
        ; AIMING AT USER, SHOOT
        JSR START_BULLET
COMP_NEXT:
        INX
        INX
        INX
        INX
        LDA TANKS_LIST+2,X
        CMP #$FF
        BCC COMP_LOOP
COMP_DONE:
        RTS
        
;==================================================
; UPDATE THE TANK POSITION
; TEMP_X = COLUMN, TEMP_Y = ROW
MOVE_N:   
        ;LDA #<TXT_NORTH 
        ;STA TML
        ;LDA #>TXT_NORTH
        ;STA TMH
        ; JSR SHOW_MSG 	; DEBUG
        LDA TEMP_Y	; GET TANK ROW
        CMP #$01
        BCC MU1
        ; JSR SHOW_ACCUM	; DEBUG
        DEC TEMP_Y
        TAX  		; SAVE OLD ROW IN THE X REGISTER
        JSR GET_POS
        LDY #$00
        LDA (TML),Y
        ; JSR SHOW_ACCUM	; DEBUG
        CMP #$20    
        BEQ MU1     ; IF BLANK DONE
        STX TEMP_Y  ; UNDO THE MOVE
MU1:    RTS
MOVE_W:        
        ;LDA #<TXT_WEST
        ;STA TML
        ;LDA #>TXT_WEST
        ;STA TMH
        ; JSR SHOW_MSG	; DEBUG

        LDA TEMP_X	; GET COLUMN
        CMP #$00
        BEQ MU2
        ; JSR SHOW_ACCUM	; DEBUG
        DEC TEMP_X
        TAX  ; SAVE OLD COL IN THE X REGISTER
        JSR GET_POS
        LDY #$00
        LDA (TML),Y
        ; JSR SHOW_ACCUM	; DEBUG
        CMP #$20    
        BEQ MU2     ; IF BLANK DONE
        STX TEMP_X  ; UNDO THE MOVE
MU2:    RTS
MOVE_S:        
        ;LDA #<TXT_SOUTH 
        ;STA TML
        ;LDA #>TXT_SOUTH
        ;STA TMH
        ; JSR SHOW_MSG

        LDA TEMP_Y	; GET TANK ROW
        CMP #MAX_Y
        BCS MU3
        ; JSR SHOW_ACCUM	; DEBUG
        INC TEMP_Y
        TAX  ; SAVE OLD ROW IN THE X REGISTER
        ; JSR SHOW_ACCUM	; DEBUG
        JSR GET_POS
        LDY #$00
        LDA (TML),Y
        ; JSR SHOW_ACCUM	; DEBUG
        CMP #$20    
        BEQ MU3     ; IF BLANK DONE
        STX TEMP_Y  ; UNDO THE MOVE
MU3:    RTS
MOVE_E:        
        ;LDA #<TXT_EAST 
        ;STA TML
        ;LDA #>TXT_EAST
        ;STA TMH
        ; JSR SHOW_MSG

        LDA TEMP_X	; GET COLUMN
        CMP #MAX_X-1
        BCS MU4
        ; JSR SHOW_ACCUM	; DEBUG
        INC TEMP_X
        TAX  ; SAVE OLD COL IN THE X REGISTER
        JSR GET_POS
        LDY #$00
        LDA (TML),Y
        ; JSR SHOW_ACCUM	; DEBUG
        CMP #$20    
        BEQ MU2     ; IF BLANK DONE
        STX TEMP_X  ; UNDO THE MOVE
MU4:    RTS

;=================================================
; LOAD SCREEN
SHOW_SCREEN:
        PHA
        LDA TEMP_X
        PHA
        LDA TEMP_Y
        PHA
        LDA #$00
        STA TEMP_X
        STA TEMP_Y
        LDY #$00
        LDX #$00
        LDA SCRN
        BNE SS_SCRN2  ; NOT SCREEN 0
SS_SCRN1:
        LDA #<SCRN_1 
        STA TML2
        LDA #>SCRN_1
        STA TMH2
        JMP SS1
SS_SCRN2:
        LDA #<SCRN_2 
        STA TML2
        LDA #>SCRN_2
        STA TMH2
        
SS1: 
        JSR GET_POS  ; TML/TMH = SCREEN ADDRESS
        LDA (TML2),Y
        CMP #$00
        BEQ SS_DONE
        CMP #$20
        BEQ SS2
        LDA #BRICK	; CHECKERED BLOCK
SS2:
        STA (TML,X)
        LDA TMH
        CLC
        INC TEMP_X
        LDA TEMP_X
        CMP #MAX_X
        BCC SS3     ; BRANCH IF A < MAX_X        
        INC TEMP_Y
        LDA #$00
        STA TEMP_X
SS3:
        INY
        BNE SS1
        INC TMH2
        JMP SS1
SS_DONE:
        PLA ; 
        STA TEMP_Y
        PLA ; 
        STA TEMP_X
        PLA ; RESTORE A
        RTS

;=================================================
; INPUT TEMP_X=COLUMN, TEMP_Y=ROW
; OUTPUT TML = SCREEN ADDRESS
GET_POS:
        PHA  	; SAVE REGISTERS
        TYA
        PHA
        LDA #<SCREEN_START
        STA TML
        LDA #>SCREEN_START
        STA TMH     
        LDA TEMP_Y
        CMP #$00
        BEQ GP3
        TAY
GP1:  
        LDA TML
        CLC
        ADC #SCR_WD   ; DOWN ONE ROW
        STA TML
        BCC GP2      
        INC TMH
GP2:
        DEY
        BNE GP1
GP3:            
        ; ADD COLUMN
        LDA TML
        CLC
        ADC TEMP_X
        STA TML
        BCC GP4
        INC TMH
GP4:
        PLA ; RESTORE REGISTERS
        TAY
        PLA
        RTS

;======================================
; CHECK AIMING AT USER TANK
; X = INVADER TANK
; RETURNS A=1 WHEN AIMIN AT USER TANK
CHK_AIM:
        LDA TANKS_LIST,X
        STA TEMP_X
        LDA TANKS_LIST ; TANK 0 X
        SEC
        SBC TEMP_X
        STA TEMP_X  ; DELTA X
        LDA TANKS_LIST+1,X
        STA TEMP_Y 
        LDA TANKS_LIST+1 ;TANK 0 Y
        SEC
        SBC TEMP_Y
        STA TEMP_Y  ; DELTA Y
                
        LDA TANKS_LIST+2,X
        ;CHECK NORTH DX=0 DY-
        CMP #$00
        BNE CA_NE
        LDA TEMP_Y
        CMP #80     
        BCC CA1 ; DELTA Y IS > 0
        LDA TEMP_X
        CMP #$02 
        BCC CA2 ; DELTA X < 2
        CMP #$FE ; -2
        BCS CA2 ; DELTA X < -2
        JMP CA_MISS
CA_NE:
        ;CHECK NORTH EAST DX+ DY-
        CMP #$01
        BNE CA_E
        LDA TEMP_Y
        CMP #80     
        BCC CA1 ; DELTA Y IS > 0
        LDA TEMP_X
        CMP #80     
        BCS CA1 ; DELTA X IS < 0
        CLC
        ADC TEMP_Y  ; A= -Y + X
        CMP #$02 
        BCC CA2 ; DELTA X < 2
        CMP #$FE ; -2
        BCS CA2 ; DELTA X < -2
        JMP CA_MISS
CA_E:
        ;CHECK EAST DX+ DY=0
        CMP #$02
        BNE CA_SE ; NOT EAST?
        LDA TEMP_X
        CMP #80     
        BCS CA1 ; DELTA X < 0?
        LDA TEMP_Y
        CMP #$02 
        BCC CA2 ; DELTA Y < 2
        CMP #$FE ; -2
        BCS CA2 ; DELTA X < -2
CA1:    JMP CA_MISS
CA2:    JMP CA_TARGET
CA_SE:
        ;CHECK SOUTH EAST DX+ DY+
        CMP #$03
        BNE CA_S
        LDA TEMP_Y
        CMP #80     
        BCS CA1 ; DELTA Y < 0
        LDA TEMP_X
        CMP #80     
        BCS CA1 ; DELTA X IS < 0
        SEC
        SBC TEMP_Y  ; A= Y - X
        CMP #$02 
        BCC CA2 ; DELTA X < 2
        CMP #$FE ; -2
        BCS CA2 ; DELTA X < -2
        JMP CA_MISS
CA_S:
        ;CHECK SOUTH DX=0 DY+
        CMP #$04
        BNE CA_SW
        LDA TEMP_Y
        CMP #80     
        BCS CA_MISS ; DELTA Y < 0
        LDA TEMP_X
        CMP #$02 
        BCC CA_TARGET ; DELTA X < 2
        CMP #$FE ; -2
        BCS CA_TARGET ; DELTA X < -2
        JMP CA_MISS
CA_SW:
        ;CHECK SOUTH WEST DX- DY+
        CMP #$05
        BNE CA_W
        LDA TEMP_Y
        CMP #80     
        BCS CA_MISS ; DELTA Y < 0
        LDA TEMP_X
        CMP #80     
        BCC CA_MISS ; DELTA X > 0
        CLC
        ADC TEMP_Y  ; A= Y + -X
        CMP #$02 
        BCC CA_TARGET ; DELTA X < 2
        CMP #$FE ; -2
        BCS CA_TARGET ; DELTA X < -2
        JMP CA_MISS
CA_W:
        ;CHECK WEST DX- DY=0
        CMP #$06
        BNE CA_NW ; NOT WEST?
        LDA TEMP_X
        CMP #80     
        BCC CA_MISS ; DELTA X > 0
        LDA TEMP_Y
        CMP #$02 
        BCC CA_TARGET ; DELTA Y < 2
        CMP #$FE ; -2
        BCS CA_TARGET ; DELTA Y < -2
        JMP CA_MISS
CA_NW:
        ;CHECK NORTH WEST DX- DY-
        CMP #$07
        BNE CA_MISS ; INVALID DIRECTION
        LDA TEMP_Y
        CMP #80     
        BCC CA_MISS ; DELTA Y IS < 0
        LDA TEMP_X
        CMP #$02 
        BCC CA_TARGET ; DELTA X < 2
        CMP #$FE ; -2
        BCS CA_TARGET ; DELTA X < -2
        JMP CA_MISS
CA_TARGET:
        LDA #$01
        RTS
CA_MISS:
        LDA #$00
        RTS
;
;======================================
; GET 8-BIT PSEUDO RANDOM NUMBER IN A.
RAND8:
        LDA R_SEED      ; GET SEED
        ASL             ; SHIFT BYTE
        BCC NO_XOR
        
        EOR #$CF       
NO_XOR:
        STA R_SEED      ; UPDATE SEED
        RTS 
;
;=======================================
; SHOW WINNER
SHOW_WINNER:
        JSR SCRAMBLE_SCR  ; CLEAR THE SCREEN
        LDA INVADERS
        BEQ SW_USER
        LDA #<TXT_LOSE 
        STA TML
        LDA #>TXT_LOSE
        STA TMH
        JMP SW_DONE
SW_USER:
        LDA #<TXT_WIN 
        STA TML
        LDA #>TXT_WIN
        STA TMH
SW_DONE:        
        JSR SHOW_MSG

        LDX #0     
        STX LAST_CR ; MAX LINE LEN ISSUE

        LDA #<TXT_AGAIN
        STA TML
        LDA #>TXT_AGAIN
        STA TMH
        JSR SHOW_MSG
        RTS
;
;======================================
; SCRAMBLE CLEAR THE SCREEN
SCRAMBLE_SCR:
        LDY #$00
LAB0_1:
        LDX #$00
LAB0_2: LDA $D000,X
        CMP #$20
        BEQ LAB1
        TYA
        STA $D000,X
LAB1:
        LDA $D100,X
        CMP #$20
        BEQ LAB2
        TYA
        STA $D100,X
LAB2:
        LDA $D200,X
        CMP #$20
        BEQ LAB3
        TYA
        STA $D200,X
LAB3:
        LDA $D300,X
        CMP #$20
        BEQ LAB4
        TYA
        STA $D300,X
LAB4:
        INX
        BNE LAB0_2    ; -47
        INY
        CPY #$21
        BNE LAB0_1    ; -54
        RTS
        BRK
;
;
;======================================
; RESET THE TANKS AND OTHER PARAMETERS.
RESET:
        LDY #$00
        LDA #<TANKS_BLANK 
        STA TML
        LDA #>TANKS_BLANK
        STA TMH
        LDA #<TANKS_LIST
        STA TML2
        LDA #>TANKS_LIST
        STA TMH2
RST1:                   
        LDA (TML),Y
        CMP #$FF
        BEQ RST2
        STA (TML2),Y 
        INY
        BNE RST1
        LDX #$00
; RESET BULLETS
RST2:
        LDA BULLETS_LIST,X
        CMP #$FF	; END OF LIST?
        BEQ RST_DONE
        LDA #$08
        STA BULLETS_LIST+2,X
        INX
        INX
        INX
        BEQ RST_DONE
        JMP RST2
RST_DONE:
        RTS

;======================================
; DISPLAY STARTUP DIRECTIONS
SHOW_DIRECTIONS:
        LDX #0     ; INIT THE INDEX.
NEXT:
        LDA DIRECTIONS,X
        BEQ NEXT2        ; DONE IF 0
        JSR CHROUT       ; OUTPUT CHR
        INX              ; NEXT
        BNE NEXT         ; LIMIT 255 
NEXT2:
        LDX #0     ; INIT THE INDEX.
        STX LAST_CR
NEXT3:
        LDA DIR2,X
        BEQ WAIT_SPACE   ; DONE IF 0
        JSR CHROUT       ; OUTPUT CHR
        INX              ; NEXT
        BNE NEXT3         ; LIMIT 255 
WAIT_SPACE: ; WAIT FOR SPACE
        INC R_SEED ; RND SEED UNGUESSABLE
        JSR GET_KBD
        CMP #$20  ; SPACE KEY?
        BNE WAIT_SPACE
DONE:
        RTS

;=================================================
; OUTPUT MESSAGE AT TML,TMH
SHOW_MSG:
        PHA
        TYA
        PHA
        LDY #$00
SM_NEXT:
        LDA (TML),Y
        BEQ SM_DONE
        JSR CHROUT
        INY
        BNE SM_NEXT
SM_DONE:
        PLA
        TAY
        PLA
        RTS

;======================================
; OUTPUT ACCUMULATER AS ASCII HEX
SHOW_ACCUM:
        PHA     ; SAVE A
        LDA #$20  ; ADD A SPACE IN FRONT OF HEX
        JSR CHROUT
        PLA    
        PHA
        LSR
        LSR
        LSR
        LSR
        JSR SA_OUT
        PLA    
        PHA
        AND #$0F
        JSR SA_OUT
        LDA #$0D        ; NEW LINE
        JSR CHROUT
        PLA ; RESTORE A
        RTS
SA_OUT:
        CMP #$0A
        BCC SA1 ; LESS THAN 0A
        ADC #$36
        JMP SA2 
SA1:    ADC #$30       
SA2:    JSR CHROUT ; OUTPUT CHARE
        RTS

;
;======================================
; DATA SECTION

R_SEED:     .BYTE   1	; SEED FOR RANDOM 
L_COUNT:    .BYTE   0  ; 
F_DONE:     .BYTE   0	; DONE FLAG	 
; TEMPORARY STORAGE FOR COORDINANTS
TEMP_X:     .BYTE $00 
TEMP_Y:     .BYTE $00 
TEMP_DIR:   .BYTE $00
USER_COUNT: .BYTE $01
INVADERS:   .BYTE $08
SCRN:       .BYTE $00 ; CURRENTLY 0 OR 1
;
; *********** GAME LISTS **************
; TANK RECORD X, Y, D, SCRN
; D = 0 TO 7 FOR DIRECTION AND 
; 8 IF TANK IS DEAD
; TANK 0 IS HUMAN TANK.
; TANKS 1 TO 4 ARE ENEMY
TANKS_LIST:
    .BYTE 3, 3, 0, 0    
    .BYTE 20, 4, 1, 0    
    .BYTE 9, 7, 3, 0    
    .BYTE 8, 15, 5, 0    
    .BYTE 16, 21, 7, 0
    .BYTE 3, 2, 1, 1    
    .BYTE 11, 7, 3, 1    
    .BYTE 18, 19, 5, 1      
    .BYTE 5, 22, 7, 1
    .BYTE $FF,$FF,$FF,$FF
TANKS_BLANK: ; RESET TO THESE VALUES
    .BYTE 3, 3, 0, 0    
    .BYTE 20, 4, 1, 0    
    .BYTE 9, 7, 3, 0    
    .BYTE 8, 15, 5, 0    
    .BYTE 16, 21, 7, 0
    .BYTE 3, 2, 1, 1    
    .BYTE 11, 7, 3, 1    
    .BYTE 18, 19, 5, 1      
    .BYTE 5, 22, 7, 1
    .BYTE $FF,$FF,$FF,$FF
;======================================
; LISTS OF 24 BULLETS X,Y,D,SCRN
; BULLET IS ALIVE IF D = 0 TO 7
BULLETS_LIST:
    .BYTE 0,0,8,0,0,8,0,0,8,0,0,8    
    .BYTE 0,0,8,0,0,8,0,0,8,0,0,8
    .BYTE 0,0,8,0,0,8,0,0,8,0,0,8
    .BYTE 0,0,8,0,0,8,0,0,8,0,0,8  
    .BYTE 0,0,8,0,0,8,0,0,8,0,0,8
    .BYTE 0,0,8,0,0,8,0,0,8,0,0,8  
    .BYTE $FF,$FF,$FF
;======================================
BULLETS: 
    .BYTE $7D, $2F, $2D, $5C, $7D, $2F, $2D, $5C

;======================================
SCRN_1:
    .BYTE "XXXXXXXXXXXXXXXXXXXXXX"
    .BYTE "X                    X"
    .BYTE "X                    X"
    .BYTE "X                    X"
    .BYTE "X   XXXXXX    XXX    X"
    .BYTE "X   X           X    X"
    .BYTE "X   X           X    X"
    .BYTE "X   X                X"
    .BYTE "X       XXXXX        X"
    .BYTE "X                    X"
    .BYTE "X   X                X"
    .BYTE "X   X                 "
    .BYTE "X   XXXX              "
    .BYTE "X            X        "
    .BYTE "X            X        "
    .BYTE "X            X       X"
    .BYTE "X      XXXXXXXXX     X"
    .BYTE "X      X             X"
    .BYTE "X      X             X"
    .BYTE "X      XXXXX         X"
    .BYTE "X          XX        X"
    .BYTE "X                    X"
    .BYTE "X                    X"
    .BYTE "XXXXXXXXXXXXXXXXXXXXXX"
    .BYTE 0
;======================================
SCRN_2:
    .BYTE "XXXXXXXXXXXXXXXXXXXXXX"
    .BYTE "X                    X"
    .BYTE "X                    X"
    .BYTE "X      XX    XX      X"
    .BYTE "X     XX      XX     X"
    .BYTE "X              XX    X"
    .BYTE "X              XX    X"
    .BYTE "X     XX      XX     X"
    .BYTE "X      XX    XX      X"
    .BYTE "X       XXXXXX       X"
    .BYTE "X                    X"
    .BYTE "                     X"
    .BYTE "                     X"
    .BYTE "       XX    XX      X"
    .BYTE "              XX     X"
    .BYTE "X             XX     X"
    .BYTE "X      XXXXXXXXX     X"
    .BYTE "X      X             X"
    .BYTE "X      X             X"
    .BYTE "X      XXXXX         X"
    .BYTE "X          XX        X"
    .BYTE "X                    X"
    .BYTE "X                    X"
    .BYTE "XXXXXXXXXXXXXXXXXXXXXX"
    .BYTE 0
;======================================
    CR = $0D
    LF = $0A
TXT_WIN:
    .BYTE CR,LF 
    .BYTE "CONGRATULATIONS, YOU"
    .BYTE CR,LF 
    .BYTE "HAVE DEFEATED THE"
    .BYTE CR,LF 
    .BYTE "INVADERS!"
    .BYTE CR,LF,0 
TXT_LOSE:
    .BYTE CR,LF 
    .BYTE "SORRY, YOUR TANK HAS"
    .BYTE CR,LF 
    .BYTE "BEEN DESTROYED!"
    .BYTE CR,LF,0 
TXT_AGAIN:
    .BYTE CR,LF 
    .BYTE "ANOTHER GAME Y/N:"
    .BYTE 0 
        
DIRECTIONS:
    .BYTE CR,LF 
    .BYTE " GOOD MORNING CAPTIAN,"
    .BYTE CR,LF 
    .BYTE CR,LF 
    .BYTE "ROBOT TANKS HAVE"
    .BYTE CR,LF 
    .BYTE "ATTACKED OUR DEFENSES."
    .BYTE CR,LF 
    .BYTE "YOU MUST DESTROY THEM!"
    .BYTE CR,LF 
    .BYTE CR,LF 
    .BYTE "YOUR CONTROLS ARE:"
    .BYTE CR,LF 
    .BYTE "[RIGHT SHIFT]", CR,LF, "      - ROTATE RIGHT"
    .BYTE CR,LF 
    .BYTE "[LEFT SHIFT]", CR,LF,  "      - ROTATE LEFT"
    .BYTE CR,LF 
    .BYTE "[BOTH SHIFTS]", CR,LF, "      - MOVE FORWARD"
    .BYTE CR,LF 
    .BYTE "[CTRL]  - SHOOT"
    .BYTE CR,LF 
    .BYTE 0      ; ZERO MARKING END OF STRING
DIR2:  ; TOO LONG DOE ONE MESSAGE.
    .BYTE CR,LF
    .BYTE "PRESS SPACE TO START."
    .BYTE 0      ; ZERO MARKING END OF STRING

