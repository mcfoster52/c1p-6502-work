# OSI C1P Projects
## A work in progress
- bin2C1P-basic: Program to convert a binary file to BASIC data statements.
- bin2C1P-hex: Program to convert a binary file to a text hex file which can be loaded from tape via the C1P monitor.
- c1p-bomber: An assembler version of the scrolling screen bomber program. (In work)
- c1p-bomber-bas: A BASIC/assembler version of the scrolling screen bomber program.
- c1p-tanks: An assembler C1P tank program.
- hello-example: A simple test program.
