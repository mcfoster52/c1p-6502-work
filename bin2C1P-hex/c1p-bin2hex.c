#include <stdio.h>

int main(int argc, char *argv[]) {
    if(argc < 2){
        fprintf(stderr, "Missing parameter, input file name.");
        return 1;
    }
    FILE *infile = fopen(argv[1],"rb");
    if (!infile) {
        fprintf (stderr, "error: file open failed '%s'.\n", argv[1]);
        return 1;
    }
    unsigned char buf[2] = {0};
    int bytes; // number of bytes read.

    // The first two bytes should be the starting address
    bytes = fread (buf, 1, 2, infile);
    if(bytes < 2){
        fprintf (stderr, "error: Expected 16 bit address at the start of the file.\n");
        return 1;
    }
    int addr = buf[0] + 256*buf[1];
    printf(".%04X/", addr); // No carriage return here.

    // Next read all the other bytes
    while ((bytes = fread (buf, 1, 1, infile)) >0) {
        printf ("%02X\n", buf[0]);
    }
    // Exit insert data mode and return to the start address
    printf(".%04XG\n", addr);

    fclose(infile);
    return 0;
}
