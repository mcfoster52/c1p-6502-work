; C1P BOMBER PILOT GAME 
; MARTIN C. FOSTER 
; cfoster@mcfse.com 
; FEBUARY, 25,2024
; NEW SHIFT SUBROUTINE FOR THE BASIC 
; BOMBER-3
; ROM SUBROUTINES 
;
SCREEN_TOP_LEFT	    = $D065
SCREEN_TOP_RT       = $D07C
SCREEN_BTM_LEFT    	= $D385
SCREEN_BTM_RT    	= $D39C
SEA_LEVEL           = $D2DC
SHIFT_COLUMNS       = SCREEN_TOP_RT - SCREEN_TOP_LEFT
SHIFT_ROWS          = 21        ; BOTTOM OF SHIFT AREA
SCORE_ADX           = SCREEN_BTM_LEFT - 32

; PAGE ZERO 
TML             = $E0
TMH             = $E1
TML2            = $E2
TMH2            = $E3

CR              = $0D
LF              = $0A


.ORG $0220  
.BYTE $22, $02  ; START ADDRESS $0300
;======================================
; CODE SECTION

;======================================
; SCREEN SHIFT ROUTINE
SHIFT:  
        PHA
        TXA
        PHA
        TYA
        PHA
        LDY #$00
LB6:    LDX #SHIFT_ROWS	; NUM ROWS DOWN.
        TYA
        PHA
        LDA #<SCREEN_TOP_LEFT
        STA TML 
        LDA #>SCREEN_TOP_LEFT
        STA TMH			; TML = TOP RIGHT SCREEN
LB7:    ; READ NEXT ROW
        INY
        LDA (TML),Y
        ; WRITE TO CURRENT COLUMN
        DEY
        STA (TML),Y
        LDA #$20 	        ; NEXT ROW: DOWN, RIGHT
        JSR PLUS
        DEX
        BNE LB7 ; WE ARE AT THE BOTTOM OF THE SHIFT AREA
        PLA      
        TAY
        INY             ; NEXT COLUMN
        CPY #SHIFT_COLUMNS
        BNE LB6         ; JUMP IF NOT LAST COLUMN
        JSR CEND
        PLA
        TAY
        PLA
        TAX
        PLA
        RTS 
; CLEARS RIGHT COLUMN
CEND:   LDY #$00
        LDX #SHIFT_ROWS	; NUM ROWS DOWN.
        LDA #<SCREEN_TOP_RT
        STA TML 
        LDA #>SCREEN_TOP_RT
        STA TMH			; TML = LOWER LEFT SCREEN
LB8:    LDA #$20	; A BLANK
        STA (TML),Y
        LDA #$20	; NEXT ROW
        JSR PLUS
        DEX
        BNE LB8
        RTS 


;======================================
; DOUBLE ADD
; TMH/TML = TMH/TML + ACCUMULATOR 
PLUS:	STA TMH2	; TMH2 = VALUE TO ADD
        CLD             ; BINARY MODE
        CLC             ; CLEAR CARRY FLAG
        LDA TML
        ADC TMH2
        STA TML
        LDA TMH
        ADC #$00 ; THIS WILL ADD THE CARRY 
        STA TMH
        CLC
        RTS
;======================================
; DOUBLE SUBTRACT
; TMH/TML = TMH/TML - ACCUMULATOR 
MINUS:  STA TMH2	; TMH2 = VALUE TO SUBTRACT
        CLD			; NOT DECIMAL MODE
        SEC 
        LDA TML
        SBC TMH2
        STA TML
        LDA TMH
        SBC #$00
        STA TMH
        CLC
        RTS


