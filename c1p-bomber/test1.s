.ORG $0220  
.BYTE $22, $02 
;======================================
; CODE SECTION
START:
    JSR INC_SCORE
    JSR INC_SCORE
    JSR INC_SCORE
    JSR INC_SCORE
    JSR SCORE_TO_TEXT
    RTS
    BRK


SCORE_TO_TEXT:
        LDY #$05
        LDX #$00
SCORE1:
        LDA SCORE,X
        PHA		; SAVE ORIGINA A
        AND #$0F
        JSR TO_ASCII
        PLA
        LSR A
        LSR A
        LSR A
        LSR A
        AND #$0F
        JSR TO_ASCII
        INX
        CPX #$03
        BCC SCORE1
        RTS

TO_ASCII:
        CLC
        ADC #$30
        STA SCORE_TXT,Y
        DEY
        RTS
		
; ADD ONE TO THE SCORE
INC_SCORE:
        PHA
        SED		; DECIMAL MODE
        CLC
        LDA SCORE
        ADC #$01
        STA SCORE
        LDA SCORE+1
        ADC #$00
        STA SCORE+1
        LDA SCORE+2
        ADC #$00
        STA SCORE+2
        CLD
        PLA
        RTS


; TEST SCORE BCD 
SCORE: 
    .BYTE   $27,$06,$00	

; ASCII SCORE
SCORE_TXT: 
    .BYTE  $00,$00,$00,$00,$00,$00,$00



