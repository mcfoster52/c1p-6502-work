; C1P BOMBER PILOT GAME 
; MARTIN C. FOSTER 
; cfoster@mcfse.com 
; FEBUARY, 25,2024
; REWRITE FROM MY OLD BASIC VERSION
; ROM SUBROUTINES 
GET_KBD     	= $FEED ; WAIT KEY TO ACCUMULATOR 
CHROUT          = $A8E5 
CTRL_C_DISABLE  = $212  ; 01 = DISABLE CTRL-C 
LAST_CR         = $0E   ; CHARACTERS SINCE LAST CR 
POLLED_KBD      = $DF00 ; 
REBOOT          = $FF00 
NEXT_ROW        = $20

; KEYBOARD CODES
KEY_RIGHT_SHFT  = $FC ; DROP BOMB
KEY_LEFT_SHFT   = $FA ; DIVE
KEY_CTRL        = $BE ; $FF CLIMB (LEFT CTRL)
KEY_NONE        = $FE
KEY_ESC         = $DE

;
SCREEN_TOP_LEFT	    = $D065
SCREEN_TOP_RT       = $D07C
SCREEN_BTM_LEFT    	= $D385
SCREEN_BTM_RT    	= $D39C
SEA_LEVEL           = $D2FC ; $D2DC
MAX_HEIGHT          = 12        ; MAXIMUM LAND HEIGHT
SHIFT_COLUMNS       = SCREEN_TOP_RT - SCREEN_TOP_LEFT
SHIFT_ROWS          = 21 ; 20        ; BOTTOM OF SHIGT AREA
SCORE_ADX           = SCREEN_BTM_LEFT - 32
PLANE_LOC_INIT      = SCREEN_TOP_LEFT + $80 + $06

; GAME CHARACTERS
PLANE_TAIL      = $D3
PLANE_FRONT     = $D4
BUILDING        = $0F
HOUSE           = $0E
TREE            = $0D
MISSLE          = $10
BUNKER          = $F3
BULLET          = $BE
BOMB            = $1B
BLOCK           = $BB

; PAGE ZERO 
TML             = $E0
TMH             = $E1
TML2            = $E2
TMH2            = $E3

CR              = $0D
LF              = $0A

; FLAGS
F_GAME_OVER     = $01
F_WINNER        = $02


.ORG $0220  
.BYTE $22, $02  ; START ADDRESS $0300
;======================================
; CODE SECTION
START:
        JSR GAME_RESET

        
GAME_START:
        JSR SHOW_DIRECTIONS
        JSR SCRAMBLE_SCR
        JSR SHOW_SCORE
        LDX #$6F
        JSR SHOW_PLANE
ST1:
        TXA
        PHA ; SAVE X
        JSR HIDE_PLANE
        JSR SHIFT
        JSR KEY_CHECK
        JSR SHOW_PLANE
        LDX #$6F
ST2:
        JSR DELAY
        DEX
        BNE ST2
        ;JSR WAIT_SHFT
        PLA ; RESTORE X
        TAX
        DEX
        BNE ST1
        ; JSR INC_SCORE
        ; JSR INC_SCORE
        ; JSR INC_SCORE
        ; JSR INC_SCORE
        ; JSR SHOW_SCORE
        JSR WAIT_SHFT
GAME_DONE:
        JSR SCRAMBLE_SCR    ; CLEAR THE SCREEN
        LDA FLAGS
        ORA #F_WINNER       ; SET WIN FLAG, YOU WIN!
        ;AND #<~F_WINNER     ; CLEAR WIN FLAG, YOU LOSE
        STA FLAGS
        JSR SHOW_WINNER
LOOP2:
        JSR GET_KBD
        CMP #$59    ; Y KEY?
        BEQ START
        CMP #$20
        BCS GAME_END ; ANY OTHER KEY
        JMP LOOP2
GAME_END:
        JMP REBOOT
        BRK

;======================================
; SCREEN SHIFT ROUTINE
SHIFT:  
        PHA
        TXA
        PHA
        TYA
        PHA
        LDY #$00
LB6:    LDX #SHIFT_ROWS	; 20 ROWS DOWN.
        TYA
        PHA
        LDA #<SCREEN_TOP_LEFT
        STA TML 
        LDA #>SCREEN_TOP_LEFT
        STA TMH			; TML = TOP RIGHT SCREEN
LB7:    ; READ NEXT ROW
        INY
        LDA (TML),Y
        ; WRITE TO CURRENT COLUMN
        DEY
        STA (TML),Y
        LDA #NEXT_ROW   ; NEXT ROW: DOWN, RIGHT
        JSR PLUS
        DEX
        BNE LB7 ; WE ARE AT THE BOTTOM OF THE SHIFT AREA
        PLA      
        TAY
        INY             ; NEXT COLUMN
        CPY #SHIFT_COLUMNS
        BNE LB6         ; JUMP IF NOT LAST COLUMN
        JSR CEND
        JSR LAND
        PLA
        TAY
        PLA
        TAX
        PLA
        RTS 
; CLEARS RIGHT COLUMN
CEND:   LDY #$00
        LDX #$14	; 20 ROWS DOWN.
        LDA #<SCREEN_TOP_RT
        STA TML 
        LDA #>SCREEN_TOP_RT
        STA TMH			; TML = LOWER LEFT SCREEN
LB8:    LDA #$20	; A BLANK
        STA (TML),Y
        LDA #NEXT_ROW 
        JSR PLUS    ; DOWN ONE LINE
        DEX
        BNE LB8
        RTS 

;======================================
; MAKE NEW LAND RIGHT COLUMN
LAND:   
        ; HEIGHT
        LDA #<SEA_LEVEL
        STA TML
        LDA #>SEA_LEVEL
        STA TMH
        LDX LAND_HEIGHT
LAND_1:
        LDY #$00
        LDA #BLOCK
        STA (TML),Y
        LDA #NEXT_ROW
        JSR MINUS ; MOVE UP ONE  LINE
        DEX
        BNE LAND_1
        ;*******************************
        ; GET THE TOP OBJECT
        ; HOUSE, TREE, MISSILE, BUNKER, OR BLANK
        JSR RAND8		; GET RANDOM NUMBER IN A.
        AND #$07 
        TAX
        LDA OBJECTS,X
        STA (TML),Y	
        ;*******************************
        ; GETNEXT LAND HEIGHT
        LDX LAND_HEIGHT
        JSR RAND8		; GET RANDOM NUMBER IN A.
        AND #$07 
        CMP #$03
        BCS LAND_4 	; JUMP IF >= 3
        ; GO DOWN
        TXA
        CMP #$02
        BCC LAND_DONE 	; DONT GO BELOW 1
        DEC LAND_HEIGHT
        JMP LAND_DONE
LAND_4:
        CMP #$05
        BCC LAND_DONE
        TXA
        CMP #MAX_HEIGHT
        BCS LAND_DONE   	; DON'T GO ABOVE MAX_HEIGHT
        INC LAND_HEIGHT
LAND_DONE:
        RTS

;======================================
; DOUBLE ADD
; TMH/TML = TMH/TML + ACCUMULATOR 
PLUS:	STA TMH2	; TMH2 = VALUE TO ADD
        CLD             ; BINARY MODE
        CLC             ; CLEAR CARRY FLAG
        LDA TML
        ADC TMH2
        STA TML
        LDA TMH
        ADC #$00 ; THIS WILL ADD THE CARRY 
        STA TMH
        CLC
        RTS

;======================================
; DOUBLE SUBTRACT
; TMH/TML = TMH/TML - ACCUMULATOR 
MINUS:  STA TMH2	; TMH2 = VALUE TO SUBTRACT
        CLD			; NOT DECIMAL MODE
        SEC 
        LDA TML
        SBC TMH2
        STA TML
        LDA TMH
        SBC #$00
        STA TMH
        CLC
        RTS

;======================================
; PROCESS KEY PRESS
KEY_CHECK:
		LDA POLLED_KBD

        ; DEBUG
    	CMP #KEY_NONE	; NO SPECIAL KEY
        BEQ KEY_DONE
        ; JSR SHOW_ACCUM
        
KEY0:	CMP #KEY_RIGHT_SHFT	; DROP BOMB?
		BNE KEY1
		; TODO: ADD A BOMB TO THE LIST AT PLANE LOCATION
		JMP KEY_DONE
KEY1:	CMP #KEY_LEFT_SHFT 	; DIVE?
		BNE KEY2
        JSR DO_DIVE
		; TODO ADD ROWHEIGHT TO PLANE LOCATION 
		; IF THE PLANE IS ABOVE MIN ALTITUDE
		JMP KEY_DONE
KEY2:	CMP #KEY_CTRL		; CLIMB?
		BNE KEY_DONE
        JSR DO_CLIMB
		; TODO SUBTRACT ROWHEIGHT FROM PLANE LOCATION 
		; IF THE PLANE IS BELOW MAX ALTITUDE

KEY_DONE:
		RTS

;======================================
; PLANE CLIMBS
DO_CLIMB:
        LDA PLANE_LOC
        STA TML
        LDA PLANE_LOC+1
        STA TMH
        LDA #<SCREEN_TOP_RT
        STA TML2
        LDA #>SCREEN_TOP_RT
        STA TMH2
        JSR COMP16
        CMP #$01    ; PLANE IS BELOW TOP?
        BNE DO_CLIME_DONE
        LDA #NEXT_ROW    ; UP ONE LINE
        JSR MINUS
        LDA TML
        STA PLANE_LOC
        LDA TMH
        STA PLANE_LOC+1
DO_CLIME_DONE:
        JSR DEBUG_PLANE
        RTS

;======================================
; PLANE DIVES
DO_DIVE:
        LDA PLANE_LOC
        STA TML
        LDA PLANE_LOC+1
        STA TMH
        LDA #<SEA_LEVEL
        STA TML2
        LDA #>SEA_LEVEL
        STA TMH2
        JSR COMP16
        CMP #$80    ; ABOVE SEA LEVEL?
        BNE DO_DIVE_DONE
        LDA #NEXT_ROW
        JSR PLUS    ; DOWN ONE LINE
        LDA TML
        STA PLANE_LOC
        LDA TMH
        STA PLANE_LOC+1
DO_DIVE_DONE:
        JSR DEBUG_PLANE
        RTS

;======================================
; TML-TMH = FIRST 16 BIT VALUE (A)
; TML2-TMH2 = SECOND 16 BIT VAL (B)
; A=0 IF A = B
; A=$01 IF A > B
; A=$80 IF A < B
COMP16:
         LDA TMH               ;MSB of 1st number
         CMP TMH2              ;MSB of 2nd number
         BCC CMP_LESS         ;A < B
;
         BNE CMP_GREATER          ;X > Y
;
         LDA TML               ;LSB of 1st number
         CMP TML2              ;LSB of 2nd number
         BCC CMP_LESS          ;X < Y
         BEQ CMP_EQUAL            ;X = Y
CMP_GREATER:
        LDA #$01
        JMP CMP_DONE
CMP_EQUAL:
        LDA #$00
        JMP CMP_DONE
CMP_LESS:
        LDA #$80
CMP_DONE:
        RTS

;======================================
; GET 8-BIT PSEUDO RANDOM NUMBER IN A.
RAND8:
        LDA R_SEED      ; GET SEED
        ASL             ; SHIFT BYTE
        BCC NO_XOR
        
        EOR #$CF       
NO_XOR:
        STA R_SEED      ; UPDATE SEED
        RTS 

;======================================
; SCRAMBLE CLEAR THE SCREEN
SCRAMBLE_SCR:
        LDY #$00
LAB0_1:
        LDX #$00
LAB0_2: LDA $D000,X
        CMP #$20
        BEQ LAB1
        TYA
        STA $D000,X
LAB1:
        LDA $D100,X
        CMP #$20
        BEQ LAB2
        TYA
        STA $D100,X
LAB2:
        LDA $D200,X
        CMP #$20
        BEQ LAB3
        TYA
        STA $D200,X
LAB3:
        LDA $D300,X
        CMP #$20
        BEQ LAB4
        TYA
        STA $D300,X
LAB4:
        INX
        BNE LAB0_2    ; -47
        INY
        CPY #$21
        BNE LAB0_1    ; -54
        RTS
        
;======================================
; JUST THIS DELAY LOOP 2.1 milli SEC
DELAY:
        PHA         ;3
        TXA         ;2
        PHA         ;3
        TYA         ;2
        PHA         ; 3
        LDX #$FF    ; 2, SUBTOTAL 15
DELAY_LOOP:  
        NOP         ; 2
        NOP         ; 2
        DEX         ; 2
        BNE DELAY_LOOP ; 2
            ; SUBTOTAL (8 * 255) + 15 = 2055
        PLA         ; 4
        TAY         ; 2
        PLA         ; 4
        TAX         ; 2
        PLA         ; 4
        RTS         ; 6 SUBTOTAL 22 + 2055 OR ABOUT 2077 uSEC

;======================================
; DISPLAY STARTUP DIRECTIONS
SHOW_DIRECTIONS:
        LDX #0     ; INIT THE INDEX.
NEXT:
        LDA DIRECTIONS,X
        BEQ NEXT2        ; DONE IF 0
        JSR WRITE_A       ; OUTPUT CHR
        INX              ; NEXT
        BNE NEXT         ; LIMIT 255 
NEXT2:
        LDX #0     ; INIT THE INDEX.
        STX LAST_CR
NEXT3:
        LDA DIR2,X
        BEQ WAIT_SHFT   ; DONE IF 0
        JSR WRITE_A       ; OUTPUT CHR
        INX              ; NEXT
        BNE NEXT3         ; LIMIT 255 
WAIT_SHFT: ; WAIT FOR SHIFT
        ROL R_SEED ; RND SEED UNGUESSABLE
        LDA POLLED_KBD      ; KEY CODE IS IN ACCUMULATOR
        CMP #KEY_NONE
        BEQ WAIT_SHFT   ; Not shift or c
DONE:
        RTS

;======================================
; WRITE THE ASCII VALUE IN THE ACCUMULATER 
; TO THE SCREEN. RESET THE LAST_CR
; WHEN A RETURN CHAR IS ENCOUNTERED.
WRITE_A:
        CMP #CR
        BNE WRITE_A1
        LDY #$00
        STY LAST_CR
WRITE_A1:
        JSR CHROUT       ; OUTPUT CHR
        RTS

;======================================
; ERASE THE PLANE
HIDE_PLANE:
        PHA ; SAVE ACCLUMULATOR
        TYA
        PHA ; SAVE Y
        LDA PLANE_LOC
        STA TML2
        LDA PLANE_LOC+1
        STA TMH2
        LDY #$00
        LDA #$20    ; BLANK
        STA (TML2),Y
        INY 
        STA (TML2),Y
        PLA ; RESTORE Y
        TAY
        PLA ; RESTORE A
        RTS
;======================================
; DRAW  THE PLANE
SHOW_PLANE:
        PHA ; SAVE A
        TYA 
        PHA ; SAVE Y
        LDA PLANE_LOC
        STA TML2
        LDA PLANE_LOC+1
        STA TMH2
        LDA #PLANE_TAIL
        LDY #$00
        STA (TML2),Y
        LDA #PLANE_FRONT
        INY
        STA (TML2),Y
        PLA ; RESTORE Y
        TAY
        PLA ; RESTORE A
        RTS

;======================================
; WRITE PLANE LOCATION TO SCREEN
; DEBUG
DEBUG_PLANE:
        PHA     ; SAVE X
        TXA
        PHA     ; SAVE A
        LDX #$00
        LDA PLANE_LOC+1
DP1:    PHA ; SAVE PLANE LOCATION BYTE
        LSR
        LSR
        LSR
        LSR
        JSR NIBBLE_TO_HEX ; GET HI NIBBLE
        STA DEBUG_TXT, X
        INX
        PLA ; RESTORE PLANE LOCATION BYTE   
        JSR NIBBLE_TO_HEX ; GET LO NIBBLE
        STA DEBUG_TXT, X
        INX
        LDA PLANE_LOC
        CPX #$03
        BCC DP1     ; BRANCH LESS
        JSR SHOW_DEBUG
        PLA    ; RESTORE X
        TAX
        PLA    ; RESTORE A


;======================================
; DRAW DEBUG MESSAGE THE SCREEN
SHOW_DEBUG:
        LDX #0     ; INIT THE INDEX.
DBG1:   LDA DEBUG_MSG,X
        BEQ DBG_DONE
        STA SCORE_ADX,X
        INX
        CPX #$17   ; TOO LONG?
        BCC DBG1
DBG_DONE:
        RTS

;======================================
; DRAW  THE SCORE TO THE SCREEN
SHOW_SCORE:
        JSR SCORE_TO_TEXT   
        LDX #0     ; INIT THE INDEX.
SS1:
        LDA SCORE_MSG,X
        BEQ SS_DONE
        STA SCORE_ADX,X
        INX
        BNE SS1
SS_DONE:
        RTS

;======================================
; CONVERT SCORE TO ASCII
SCORE_TO_TEXT:
        LDY #$05
        LDX #$00
SCORE1:
        LDA SCORE,X
        PHA		; SAVE ORIGINA A
        AND #$0F
        JSR TO_ASCII
        PLA
        LSR A
        LSR A
        LSR A
        LSR A
        AND #$0F
        JSR TO_ASCII
        INX
        CPX #$03
        BCC SCORE1
        RTS

TO_ASCII:
        CLC
        ADC #$30
        STA SCORE_TXT,Y
        DEY
        RTS
		
;======================================
; ADD ONE TO THE SCORE
INC_SCORE:
        PHA
        SED		; DECIMAL MODE
        CLC
        LDA SCORE
        ADC #$01
        STA SCORE
        LDA SCORE+1
        ADC #$00
        STA SCORE+1
        LDA SCORE+2
        ADC #$00
        STA SCORE+2
        CLD
        PLA
        RTS

;=======================================
; RESET ALL GAME DATA
GAME_RESET:
        LDA #$00
        STA FLAGS   ; RESET ALL FLAGS
        STA SCORE
        STA SCORE+1
        STA SCORE+2 ; ZERO OUT THE  SCORE
        LDA #$01
        STA LAND_HEIGHT
        LDA #<PLANE_LOC_INIT
        STA PLANE_LOC
        LDA #>PLANE_LOC_INIT
        STA PLANE_LOC+1

; RESET BULLETS
        LDX #$00
RST1:
        LDA BULLETS_LIST,X
        CMP #$FF	; END OF LIST?
        BEQ RST2
        LDA #$00
        STA BULLETS_LIST,X
        INX
        STA BULLETS_LIST,X
        INX
        STA BULLETS_LIST,X
        INX
        BNE RST1
RST2:
        LDX #$00
RST2_1:
        LDA MISSLE_LIST,X
        CMP #$FF	; END OF LIST?
        BEQ RST3
        LDA #$00
        STA MISSLE_LIST,X
        INX
        STA MISSLE_LIST,X
        INX
        STA MISSLE_LIST,X
        INX
        BNE RST2_1
RST3:
        LDX #$00
RST3_1:
        LDA BOMB_LIST,X
        CMP #$FF	; END OF LIST?
        BEQ RST_DONE
        LDA #$00
        STA BOMB_LIST,X
        INX
        STA BOMB_LIST,X
        INX
        STA BOMB_LIST,X
        INX
        BNE RST3_1
RST_DONE:
        RTS

;=======================================
; SHOW WINNER
SHOW_WINNER:
        LDA FLAGS
        AND #F_WINNER
        BNE SW_WIN
        LDA #<TXT_LOSE 
        STA TML
        LDA #>TXT_LOSE
        STA TMH
        JMP SW_DONE
SW_WIN:
        LDA #<TXT_WIN 
        STA TML
        LDA #>TXT_WIN
        STA TMH
SW_DONE:        
        JSR SHOW_MSG

        LDX #0     
        STX LAST_CR ; MAX LINE LEN ISSUE

        LDA #<TXT_AGAIN
        STA TML
        LDA #>TXT_AGAIN
        STA TMH
        JSR SHOW_MSG
        RTS

;=================================================
; OUTPUT MESSAGE AT TML,TMH
SHOW_MSG:
        PHA
        TYA
        PHA
        LDY #$00
SM_NEXT:
        LDA (TML),Y
        BEQ SM_DONE
        JSR CHROUT
        INY
        BNE SM_NEXT
SM_DONE:
        PLA
        TAY
        PLA
        RTS
;======================================
; OUTPUT ACCUMULATER AS ASCII HEX
SHOW_ACCUM:
        PHA     ; SAVE A
        LDA #$20  ; ADD A SPACE IN FRONT OF HEX
        JSR CHROUT
        PLA    
        PHA
        LSR
        LSR
        LSR
        LSR
        JSR SA_OUT ; PRINT HI NIBBLE
        PLA    
        PHA
        JSR SA_OUT ; PRINT LO NIBBLE
        LDA #$0D   ; START OF LINE
        JSR CHROUT
        PLA ; RESTORE A
        RTS
SA_OUT:
        JSR NIBBLE_TO_HEX
SA2:    JSR CHROUT ; OUTPUT CHARE
        RTS

;======================================
; CONVERT LOW NIBBLE IN ACCUMULATER 
; TO ASCII HEX IN ACCUMULATER.  
NIBBLE_TO_HEX:
        AND #$0F
        CMP #$0A
        BCC SA1 ; LESS THAN 0A
        ADC #$36
        JMP SA2 
SA1:    ADC #$30       
        RTS

;======================================
; DATA SECTION
LAND_HEIGHT:
	.BYTE	$01		; CURRENT HEIGHT 1 THROUGH MAX_HEIGHT
; START PLANE 4 ROWS DOWN, COLUMN 6
PLANE_LOC:
    .WORD PLANE_LOC_INIT

R_SEED:
	.BYTE	$55		; CURRENT HEIGHT 1 THROUGH MAX_HEIGHT

OBJECTS: 
    .BYTE HOUSE		; 0
    .BYTE TREE		; 1
    .BYTE MISSLE	; 2
    .BYTE BUNKER		; 3
    .BYTE BUILDING		; 4
    .BYTE 32,32,32,32		; 4,5,6,7

TXT_WIN:
    .BYTE CR,LF 
    .BYTE "CONGRATULATIONS, YOU"
    .BYTE CR,LF 
    .BYTE "HAVE COMPLETED YOUR"
    .BYTE CR,LF 
    .BYTE "MISSION!"
    .BYTE CR,LF,0 
TXT_LOSE:
    .BYTE CR,LF 
    .BYTE "SORRY, YOUR PLANE HAS"
    .BYTE CR,LF 
    .BYTE "BEEN DESTROYED!"
    .BYTE CR,LF,0 
TXT_AGAIN:
    .BYTE CR,LF 
    .BYTE "ANOTHER GAME Y/N:"
    .BYTE 0 

DIRECTIONS:
    .BYTE CR,LF,CR,LF,CR,LF
    .BYTE "HELLO CAPTIAN,",CR,LF,CR,LF 
    .BYTE "YOU ARE TO FLY A LOW", CR,LF 
    .BYTE "LEVEL BOMBING MISSION",CR,LF
    .BYTE "DEEP INTO ENEMY  ",CR,LF 
    .BYTE "TERRITORY!",CR,LF 
    .BYTE CR,LF 
    .BYTE "YOU MUST DESTROY THE",CR,LF 
    .BYTE "MISSILES & BUNKERS.",CR,LF 
    .BYTE "AVOID HOUSES & TREES.",CR,LF,CR,LF
    .BYTE "CONTROLS ARE:",CR,LF 
    .BYTE 0      ; ZERO MARKING END OF STRING
DIR2:  ; TOO LONG FOR ONE MESSAGE.
    .BYTE CR,LF
    .BYTE "RT SHIFT  - DROP BOMB",CR,LF 
    .BYTE "LFT SHIFT - DIVE",CR,LF 
    .BYTE "LFT CTRL  - CLIMB",CR,LF 
    .BYTE CR,LF
    .BYTE CR,LF
    .BYTE "PRESS SHIFT TO START"
    .BYTE 0      ; ZERO MARKING END OF STRING

;======================================
; UP TO 8 BULLETS, MISSLES, AND BOMBS
; ADDRESS_LO, ADDRESS_HI, F
; BULLET IS ALIVE IF F IS NOT 0 OR $FF
BULLETS_LIST:
    .BYTE 0,0,0, 0,0,0, 0,0,0, 0,0,0    
    .BYTE 0,0,0, 0,0,0, 0,0,0, 0,0,0    
    .BYTE $FF,$FF,$FF
MISSLE_LIST:
    .BYTE 0,0,0, 0,0,0, 0,0,0, 0,0,0    
    .BYTE 0,0,0, 0,0,0, 0,0,0, 0,0,0    
    .BYTE $FF,$FF,$FF
BOMB_LIST:
    .BYTE 0,0,0, 0,0,0, 0,0,0, 0,0,0    
    .BYTE 0,0,0, 0,0,0, 0,0,0, 0,0,0    
    .BYTE $FF,$FF,$FF
FLAGS:
    .BYTE $00

; SCORE BCD 
SCORE: 
    .BYTE   $00,$00,$00	
; ASCII SCORE
SCORE_MSG:
    .BYTE "SCORE: "
SCORE_TXT: 
    .BYTE  $00,$00,$00,$00,$00,$00,$00
; ASCII DEBUG TEXT
DEBUG_MSG:
    .BYTE "DEBUG: "
    ; SET A DEBUG MESSAGE HERE.
DEBUG_TXT: 
    .BYTE  $00,$00,$00,$00,$00,$00,$00

