cl65 -t none -l $1.lst -o $1.bin $1.s
c1p-bin2hex $1.bin > $1.hex
# if code is small enough
#c1p-bin2bas $1.bin > $1.bas 
# make a tape file
#kcs_encode.py $1.hex $1.wav
# Cleanup, remove object file
rm $1.o
